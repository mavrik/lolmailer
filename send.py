#!/usr/bin/env python
# -*- coding: utf-8 -*-
from jinja2 import Template, Environment, FileSystemLoader
from model import get_undispatched_articles
import itertools

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
import requests
import settings

env = Environment(loader=FileSystemLoader("template"))

def send_mails():
    mail, images = render_mail()
    send_mail(settings.MAIL_RECIPIENTS, mail, images)

def send_mail(recipient, content, images):
    strFrom = "lolmailer@virag.si"
    strTo = recipient

    # Root message
    msgRoot = MIMEMultipart("related")
    msgRoot["Subject"] = "Your daily kitten shipment!"
    msgRoot["From"] = strFrom
    msgRoot["To"] = ", ".join(strTo)

    msgText = MIMEText(content, 'html', 'utf-8')
    msgRoot.attach(msgText)

    for image_id, image_url in images:
        print "Attaching", image_url
        image = requests.get(image_url)
        mime_img = MIMEImage(image.content)
        mime_img.add_header("Content-ID", str(image_id))
        msgRoot.attach(mime_img)

    smtp = smtplib.SMTP()
    smtp.connect(settings.SMTP_ADDRESS, settings.SMTP_PORT)
    if settings.SMTP_USERNAME:
        smtp.login(settings.SMTP_USERNAME, settings.SMTP_PASSWORD)
    smtp.sendmail(strFrom, strTo, msgRoot.as_string())
    smtp.quit()

def render_mail():
    articles = get_undispatched_articles(mark_dispatched=True)
   
    images = []
    for id, article in itertools.izip(itertools.count(), articles):
        article["img_id"] = id
        images.append((id, article["img_url"]))

    template = env.get_template("mail.html")
    rendered_mail = template.render(articles=articles)
    return rendered_mail, images

if __name__ == "__main__":
    send_mails()
