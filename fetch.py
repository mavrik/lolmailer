#!/usr/bin/env python
import parsers
from model import store_articles

if __name__ == "__main__":
    sites = parsers.get_parsers()
    for site in sites:
        content = site.get_data()
        store_articles(content)
