from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, String, Integer, Boolean, Unicode, create_engine
import settings

Base = declarative_base()
Session = sessionmaker()
engine = create_engine(settings.DB_CONNECTION_STRING, echo=True)
Session.configure(bind=engine)

class Article(Base):
    __tablename__ = "articles"

    id = Column(Integer, primary_key=True)
    site = Column(String)
    title = Column(Unicode)
    img_url = Column(String, index=True, unique=True)
    subtext = Column(Unicode)
    sort = Column(Integer, default=99999)
    dispatched = Column(Boolean)

Base.metadata.create_all(engine)

def store_articles(articles):
    session = Session()

    # Check existing URLs
    urls = [article["img_url"] for article in articles]
    existing_urls = set([url[0] for url in session.query(Article.img_url).filter(Article.img_url.in_(urls)).all()])

    for article in articles:
        if article["img_url"] in existing_urls:
            continue

        db_article = Article(site=article["site"], title=article.get("title", None), img_url=article["img_url"], 
                             subtext=article.get("subtext", None), sort=article.get("sort", 99999), dispatched=False)
        session.add(db_article)
    session.commit()

def get_undispatched_articles(mark_dispatched=False):
    session = Session()
    db_articles = session.query(Article).filter_by(dispatched=False).order_by(Article.sort).all()
    articles = [ { "site": article.site, "title" : article.title, "img_url" : article.img_url, "subtext":article.subtext } for article in db_articles ]
    if mark_dispatched:
        for article in db_articles:
            article.dispatched = True
            session.add(article)
        session.commit()
    
    return articles 
