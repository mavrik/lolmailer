import bs4
import requests

class ICanHazParser(object):
    URL = "http://icanhas.cheezburger.com/"

    def get_data(self):
        posts = []
        
        site_data = requests.get(self.URL)
        site = bs4.BeautifulSoup(site_data.text)

        post_divs = site.findAll(class_="event-item-lol-image")
        for post in post_divs:
            title = post["title"]
            url = post["src"]
            posts.append({ "title" : title, "img_url" : url, "site":"ICanHazCheezburger", "sort" : 300 })
        return posts


