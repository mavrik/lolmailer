import parsers.icanhaz
import parsers.bugcomic
import parsers.xkcd

parsers = [ icanhaz.ICanHazParser(), bugcomic.BugComicParser(), xkcd.XkcdParser(), ]

def get_parsers():
    return parsers
