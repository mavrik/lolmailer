import feedparser
import bs4

class XkcdParser(object):
    RSS_URL = "http://xkcd.com/rss.xml"

    def get_data(self):
        feed = feedparser.parse(self.RSS_URL)

        # Grab last entry
        title = feed.entries[0].title
        last_entry = bs4.BeautifulSoup(feed.entries[0].summary)
        subtext = last_entry.img["alt"]
        img_url = last_entry.img["src"]

        return [ { "title" : title, "subtext": subtext, "img_url":img_url, "site":"XKCD", "sort" : 25 }]
