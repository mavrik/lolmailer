import feedparser
import bs4


class BugComicParser(object):
    RSS_URL = "http://www.bugcomic.com/feed/"

    def get_data(self):
        # Grab only the final entry
        feed = feedparser.parse(self.RSS_URL)
        last_comic = feed.entries[0]
        content = bs4.BeautifulSoup(last_comic.content[0].value)

        title = last_comic.title
        img_url = content.img["src"]

        return [{ "title" : title, "img_url" : img_url, "site": "BugComic", "sort": 50 }]
